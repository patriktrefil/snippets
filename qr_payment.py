#!/bin/env python3
# recommended for (CZ) inland payments only
# script that makes an API request for specified payment template

import requests

accountNumber = "222885"
bankCode = "5500"
amount = "250.00"
currency = "CZK"
variableSymbol = "333"
message = "hello world"

url = "http://api.paylibo.com/paylibo/generator/czech/image?" + \
        "accountNumber=" + accountNumber + \
        "&bankCode=" + bankCode + \
        "&amount=" + amount + \
        "&currency=" + currency + \
        "&vs=" + variableSymbol + \
        "&message=" + message

response = requests.get(url)

if response.status_code == 200:

        with open("sample.jpg", 'wb') as f:

                    f.write(response.content)
