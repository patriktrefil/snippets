#!/usr/bin/env python3

from blessed import Terminal
import sys
import random
import math

term = Terminal()

def random_mess():
    res = ""
    for y in range(term.height):
            for x in range(term.width):
                if random.randint(0, 1):
                    res += term.on_color_rgb(0, 0, 0) + " "
                else:
                    res += term.on_color_rgb(random.randrange(255), random.randrange(255) ,random.randrange(255)) + " "
    return res

def stripes():
    res = ""
    for y in range(term.height):
            for x in range(term.width):
                if x % 2:
                    res += term.on_color_rgb(255, 0, 0) + " "
                else:
                    res += term.on_color_rgb(0, 0, 0) + " "
    return res

def circles():
    res = ""
    for y in range(term.height):
            for x in range(term.width):
                if abs(math.sqrt(x * x + y * y) - 150) <= 0.5:
                    res += term.on_color_rgb(255, 0, 0) + " "
                else:
                    res += term.on_color_rgb(0, 0, 0) + " "
    return res

def tan():

    return False
    res = ""
    for y in range(term.height):
            for x in range(term.width):
                if math.tan(x) > 0:
                    res += term.on_color_rgb(255, 0, 0) + " "
                else:
                    res += term.on_color_rgb(0, 0, 0) + " "
    return res


def refresh():
    def je_tam(x, y):
        for item in souradnice:
            if item.x == x and item.y == y:
                return True
    res = ""
    for y in range(term.height):
            for x in range(term.width):
                if je_tam(x, y):
                    res += term.on_color_rgb(random.randrange(255), random.randrange(255), random.randrange(255)) + " "
                else:
                    res += term.on_color_rgb(0, 0, 0) + " "
    return res


class Bod:
    def __init__(self, x, y):
        self.x = x
        self.y = y


souradnice = []
for i in range(100):
    souradnice += [Bod(random.randint(0, term.width), random.randint(0, term.height))]

with term.hidden_cursor(), term.fullscreen(), term.cbreak():
    while True:
        res = refresh()
        for item in souradnice:
            item.x = (item.x + random.randint(-1, 1)) % term.width
            item.y = (item.y + random.randint(-1, 1)) % term.height

        print(res, end="")

        if term.inkey(timeout=0.1) == "q":
            break

        sys.stdout.flush()
